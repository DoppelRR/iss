
/**
 * Convolution Filter
 * 
 * @author Can Arslan
 */
public class Convolution3x3_CA extends ConvolutionFilter_CA {

	@Override
	public double[][] getKernel() {
		//Hier sind die filterwerte eingegeben
		double[][] filter  = {{0.111,0.111,0.111},{0.111,0.111,0.111},{0.111,0.111,0.111}};
		return filter;
	}
	

}
