import itb2.filter.AbstractFilter;
import itb2.filter.RequireImageType;
import itb2.image.GrayscaleImage;
import itb2.image.Image;
import itb2.image.ImageConverter;
import itb2.image.ImageFactory;
import itb2.image.RgbImage;
import java.lang.Math;

/**
 * Gamma-Korrektur für Grauwertbilder
 * 
 * @author Lennart Metz
 *
 */


@RequireImageType(GrayscaleImage.class)
public class Gammakorrektur_CA_CW_LM extends AbstractFilter {
	
	private static final String GAMMA = "Gamma";
	
	public B2_A1_CA_CW_LM() {
		properties.addDoubleProperty(GAMMA, 1);
	}
	
	@Override
	public Image filter(Image input) {
		GrayscaleImage output = ImageFactory.bytePrecision().gray(input.getSize());
		//ueber pixel iterieren
		for(int col = 0; col < input.getWidth(); col++) {
			for(int row = 0; row < input.getHeight(); row++) {
				//Gamma-Korrektur jedes Pixels mit Ng = 256, Imax = 255, Imin = 0
				int t = (int) Math.round(256*Math.pow(input.getValue(col, row, 0)/255, properties.getDoubleProperty(GAMMA)));
				input.setValue(col, row, t);
			}
		}
		return output;
	}
}


/*
B.: 
Gamma = 0.3
Die Korrekturfunktion steigt zunaechst stark an und flacht dann bis zum ende immer weiter ab. Dies entspricht einer Streckung der niedrigen
Intensitaetswerte und einer Stauchung der hohen. Im Bild ist zu beobachten das viele dunkle Bereiche aufgehellt wurden, ihre Intensitaetswerte
also erhoeht wurden. Lediglich die Pixel ganz am Intensitaetsminimum, im Bild die Haare, bleiben dunkel. Im Gegensatz dazu haben helle Bereiche,
z.B. das Kleid, an Kontrast verloren, da die hohen Intensitaeten auf einen kleineren Bereich abgebildet wurden.
Gamma = 1
Das Bild ist unveraendert, was der Korrekturfunktion entspricht die alle Intensitaetswerte auf sich selbst abbildet.
Gamma = 3
Die Korrekturfunktion bildet niedrige Intensiaetswerte auf einen kleineren Bereich ab und streckt gleichzeitig die hohen Werte.
Das Bild ist sehr dunkel, also sind die Mehrzahl der Pixel bereits im niedrigen Intensitaetsbereich
und werden durch den Filter noch weiter abgedunkelt wodurch Teile des Bilds kaum noch zu erkennen sind. Die hellen Teile des Bildes werden im Kontrast verstaerkt,
da ihre Intensitaeten auf ein groeßeres Spektrum abgebildet werden. Dies ist am Kleid gut zu erkennen.
*/