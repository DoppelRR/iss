
/**
 * Convolution Filter
 * 
 * @author Can Arslan
 */
public class Convolution5x5_CA extends ConvolutionFilter_CA {

	@Override
	public double[][] getKernel() {
		//Hier sind die filterwerte eingegeben
		double[][] filter = {{0.04,0.04,0.04,0.04,0.04},{0.04,0.04,0.04,0.04,0.04},{0.04,0.04,0.04,0.04,0.04},{0.04,0.04,0.04,0.04,0.04},{0.04,0.04,0.04,0.04,0.04}};
		return filter;
	}
	

}
