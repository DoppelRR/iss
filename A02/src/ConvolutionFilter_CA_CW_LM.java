import itb2.filter.AbstractFilter;
import itb2.image.Image;
import itb2.image.ImageFactory;


/**
 * Convolution Filter
 * 
 * @author Can Arslan
 */

public abstract class ConvolutionFilter_CA extends AbstractFilter{

	public ConvolutionFilter_CA() {
		
	}
	@Override
	public Image filter(Image input) {
		//hier wird der filterkern aus der unterklasse abgefragt
		double[][] filter = getKernel(); 
		//das neue feld wird am anfang erstmal mit 0 initialisiert. es handelt sich hier um die summe des zu berechnenden pixels
		double newValue = 0;
		//f�r eine funktionierende kalkulation m�ssen wir berchnen wie gro� unser offset maximal ist. bei einer 5x5-convolution, ist das von -2 bis 2
		int len = filter[0].length;
		
		Image output = ImageFactory.getPrecision(input).gray(input.getSize());
	    
		//iterieren erst �ber die spalten und dann �ber die reihen.
		for(int col = 0; col < input.getWidth(); col++) {
			for(int row = 0; row < input.getHeight(); row++) {
				newValue = 0;	
				//der initiale offset f�r die "obere linke ecke" des filters wird hier berechnet.
				int colOffset = col-((len-1)/2);
				int rowOffset = row-((len-1)/2);
				//dann iterieren wir anfangend bei der oberen linken ecke spalten und zeilenweise durch den filter
				for(int u = 0; u < len; u++) {
					for(int v = 0; v < len; v++) {
						try {						
							//und versuchen jedes mal alle pixel von der maske bedecken zu lassen.
							newValue += input.getValue((colOffset+u), rowOffset + v,0) * filter[u][v];
							}
						//manchmal geht es aber nicht, und zwar alle pixel die <=len pixel vom rand entfernt sind. hier f�hrt das zu einer nullpointer-exeption
						catch (Exception e){
						//Wenn das eingetreten sein sollte, nimm einfach das pixel das grade von row und col angegeben ist und ersetze fehlende pixel mit ihm.
							newValue += input.getValue(col, row, 0)*filter[u][v] ;
						}
						}
					}
				//gib das bild aus.
				output.setValue(col, row, Math.round(newValue));
				}
			}
		
		
		
		return output;
	}

	public abstract double[][] getKernel();	
}