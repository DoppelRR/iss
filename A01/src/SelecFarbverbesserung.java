import itb2.filter.AbstractFilter;
import itb2.image.Image;
import itb2.image.ImageConverter;
import itb2.image.ImageFactory;
import itb2.image.RgbImage;

/**
 * Exemplary filter showing all property types
 * 
 * @author Micha Strauch
 */
public class SelecFarbverbesserung extends AbstractFilter {
	
	/** Property names */
	private static final String CHANNEL = "Option Property", SCALE = "Double Property";
	
	/** Constructor, setting the default values for the properties */
	public SelecFarbverbesserung() {
		ImageConverter.register(RgbImage.class, ImageFactory.bytePrecision().gray(), this);
		properties.addDoubleProperty(SCALE, 1.0);
		properties.addOptionProperty(CHANNEL, "RED", "RED", "GREEN", "BLUE");
	}
	
	@Override
	public Image filter(Image input) {
		
		// Ausgabebild erstellen
		Image output = ImageFactory.bytePrecision().rgb(input.getSize());
		

		double a = properties.getDoubleProperty(SCALE);
		if (a > 10)
			a = 10;
		else if (a < 0)
			a = 0;

		String k = properties.getOptionProperty(CHANNEL).toString();
		double red = k == "RED" ? a : 1;
		double green = k == "GREEN" ? a : 1;;
		double blue = k == "BLUE" ? a : 1;

		// Über Pixel iterieren
		for(int col = 0; col < input.getWidth(); col++) {
			for(int row = 0; row < input.getHeight(); row++) {
				output.setValue(col, row, 0, red * input.getValue(col, row, 0));
				output.setValue(col, row, 1, green * input.getValue(col, row, 1));
				output.setValue(col, row, 2, blue * input.getValue(col, row, 2));
			}
		}
		
		return output;
		
	}
	
}