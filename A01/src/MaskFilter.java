import itb2.engine.Controller;
import itb2.filter.AbstractFilter;
import itb2.image.Image;
import itb2.image.ImageConverter;
import itb2.image.ImageFactory;
import itb2.image.RgbImage;

/**
 * Exemplary filter showing all property types
 * 
 * @author Micha 
 */
public class MaskFilter extends AbstractFilter {
	
	/** Constructor, setting the default values for the properties */
	public MaskFilter() {
		ImageConverter.register(RgbImage.class, ImageFactory.bytePrecision().gray(), this);
	}
	
	@Override
	public Image[] filter(Image[] input) {
        Image picture = input[0];
        Image mask = input [1];

        if (picture.getWidth() != mask.getWidth() || picture.getHeight() != mask.getHeight()) {
            Controller.getCommunicationManager().error("Picture and Mask, have to be the same size.");
            return null;
        }

		
		// Ausgabebild erstellen
		Image output = ImageFactory.bytePrecision().rgb(picture.getSize());

		// Über Pixel iterieren
		for(int col = 0; col < picture.getWidth(); col++) {
			for(int row = 0; row < picture.getHeight(); row++) {
                for (int chann = 0; chann < picture.getChannelCount(); chann++) {
                    output.setValue(col, row, chann, mask.getValue(col, row, chann) > 0 ? picture.getValue(col, row, chann) : 0);
                }
			}
		}

		return new Image[]{output};
		
	}
	
}