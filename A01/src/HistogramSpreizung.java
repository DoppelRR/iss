public class HistogramSpreizung {
    public static void main(String[] args) {

        //*
        int[] intensitäten = {2, 3, 3, 5, 
            2, 4, 4, 5};
        //*/

        double c1 = -2;
        double c2 = 7.0 / 3.0;

        System.out.println("Gespreitztes Histogram:");
        for (int i : intensitäten) {
            System.out.println(Math.round((i + c1) * c2));
        }
    }
}
