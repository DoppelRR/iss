public class GammaSpreizung {
    public static void main(String[] args) {

        //*
        int[] intensitäten = {0, 1, 1, 4, 
            0, 2, 2, 4};
        //*/

        double gamma = 2.0;
        double iMin = 0;
        double iMax = 7;
        double ng = iMax + 1;

        System.out.println("Gespreitztes Histogram:");
        for (int i : intensitäten) {
            double result = (i - iMin) /  (iMax - iMin);
            result = Math.pow(result, gamma);
            result *= ng;
            result += iMin;
            result = Math.round(result);
            result = Math.min(result, iMax); //Sollte result größer sein als iMax wird stattdessen iMax zurückgegeben

            System.out.println(result);
        }
    }
}
