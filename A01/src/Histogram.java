import java.util.HashMap;

public class Histogram {

    public static void main(String[] args) {
        /*
        int[] intensitäten = {4, 2, 5, 1, 
                              5, 3, 2, 3, 
                              4, 2, 6, 2,
                              4, 1, 2, 1};
        //*/

        /*
        int[] intensitäten = {2, 1, 2, 1, 
                              1, 7, 6, 1, 
                              0, 6, 6, 2,
                              1, 1, 2, 1};
        //*/

        /*
        int[] intensitäten = {2, 3, 3, 5, 
            2, 4, 4, 5};
        //*/

        //*
        int[] intensitäten = {0, 1, 1, 7, 
            2, 6, 6, 7};
        //*/

        HashMap<Integer, Integer> histogram = new HashMap<Integer, Integer>();
        HashMap<Integer, Double> histogramNormalized = new HashMap<Integer, Double>();

        for (int i : intensitäten) {
            histogram.put(i, histogram.getOrDefault(i, 0) + 1);
        }
        
        for (int i : histogram.keySet()) {
            histogramNormalized.put(i, (double) histogram.get(i) / intensitäten.length);
        }

        double mittelwert = 0;
        for (int i : histogram.keySet()) {
            mittelwert += i * histogramNormalized.get(i);
        }

        double abweichung = 0;
        for (int i : histogram.keySet()) {
            abweichung += Math.pow(i - mittelwert, 2) * histogramNormalized.get(i);
        }
        
        
        System.out.println("Histogram:");
        for (int i : histogram.keySet()) {
            System.out.println(i + ": " + histogram.get(i));
        }

        System.out.println("Histogram normalisiert:");
        for (int i : histogram.keySet()) {
            System.out.println(i + ": " + histogramNormalized.get(i));
        }

        System.out.println("Mittelwert: " + mittelwert);
        System.out.println("Abweichung: " + abweichung);
    }

}