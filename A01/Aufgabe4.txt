Zur Rechnung siehe .\src\Histogram.java

1.
Intensität | Anzahl I1 | Anzahl I2
-----------+-----------+----------
0          | 0         | 1
1          | 3         | 7
2          | 5         | 4 
3          | 2         | 0
4          | 3         | 0
5          | 2         | 0
6          | 1         | 3
7          | 0         | 1

2.
Intensität | Normalisiert I1 | Normalisiert I2
-----------+-----------------+-----------------
0          | 0               | 0.0625
1          | 0.1875          | 0.4375
2          | 0.3125          | 0.25
3          | 0.125           | 0
4          | 0.1875          | 0
5          | 0.125           | 0
6          | 0.0625          | 0.1875
7          | 0               | 0.0625

3.

Mittelwert I1: 2.9375
Mittelwert I2: 2.5

Quadratische Abweichung I1: 2.30859375
Quadratische Abweichung I2: 5.0

4.
Die Quadratische Abweichung in Bild 2 ist wesentlich höher.

