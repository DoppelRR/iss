
a)
Intensität | Anzahl
-----------+-------
0          | 0
1          | 0
2          | 2 
3          | 2
4          | 2
5          | 2
6          | 0
7          | 0

b)
Intensität | Normalisiert
-----------+-------------
0          | 0
1          | 0
2          | 0.25
3          | 0.25 
4          | 0.25
5          | 0.25 
6          | 0
7          | 0

c)

c1 = -IminGiven. Also hier -2. Hierdurch verschiebebn wir das Histogram soweit nach links wie möglich.
c2 = (Imax - Imin) / (ImaxGiven - IminGiven) = (7 - 0)/(5 - 2) = 7 / 3 ~= 2,33

d)
Für die Rechnung siehe .\src\HistogramSpreizung.java

Neues Grauwertbild:
-----------------
| 0 | 2 | 2 | 7 |
-----------------
| 0 | 5 | 5 | 7 |
-----------------